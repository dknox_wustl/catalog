export default function filterquery (paramsarg) {
    let s = "";
    let params = {...paramsarg};
    delete(params["q"]);
    delete(params["sortdir"]);
    delete(params["sortby"]);
    let datefilter = "";

    if ("from" in params) {
        if ("to" in params) {
            s += `&fq=eppublicationYear_txt:[${params["from"]} TO ${params["to"]}]`;
            delete(params["to"]);
        } else {
            s += `&fq=eppublicationYear_txt:${params["from"]}`;
        }
        delete(params["from"]);
    } 
    if ("to" in params) {
        delete(params["to"]);
    }
    for (var field in params) {
        let vals = params[field].split("|");
        for (var i in vals) {
            let val = decodeURI(vals[i]);
            let fq = `&fq=${field}:"${val}"`;
            if (val.indexOf(" TO ") >= 0) {
                fq = `&fq=${field}:${val}`;
            } 
            s += fq;
        }
    }
    s += datefilter;
    return s;
}

/*
export default function filterquery (filters) {
    let s = "";
    for (var field in filters) {
        for (var i in filters[field]) {
            let val = filters[field][i];
            let fq = `&fq=${field}:"${val}"`;
            s += fq;
        }
    }
    return s;
}
*/
