import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import About from "./views/About.vue";
import DashOrig from "./views/DashOrig.vue";
import Catalog from "./views/Catalog.vue";
import Inventory from "./views/Inventory.vue";
import InventorySplit from "./views/InventorySplit.vue";
import Inventories from "./views/Inventories.vue";
// import Doc from "./components/Doc.vue";
import Compare2 from "./components/Compare2.vue";
import Field from "./components/Field.vue";
import OrigTerm from "./components/OrigTerm.vue";
import TextWrap from "./components/TextWrap.vue";
import MltWrap from "./components/MltWrap.vue";

Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "catalogHome",
            component: Catalog
        },
        {
            path: "/catalog",
            name: "catalog",
            component: Catalog
        },
        {
            path: "/doc/:id",
            name: "doc",
            // component: Doc,
            component: () => import(/* webpackChunkName: "doc" */ "./components/Doc.vue")
        },
        {
            path: "/about",
            name: "about",
            component: About
        },
        {
            path: "/inventories",
            name: "inventories",
            component: Inventories
        },
        {
            path: "/inventory",
            name: "inventory",
            component: Inventory
        },
        {
            path: "/inventorysplit",
            name: "inventorysplit",
            component: InventorySplit
        },
        {
            path: "/home",
            name: "home",
            component: Home
        },
        {
            path: "/docold/:id",
            name: "docold",
            // component: Doc,
            component: () => import(/* webpackChunkName: "doc" */ "./components/Doc_old.vue")
        },
        {
            path: "/field/:fieldname",
            name: "fieldname",
            component: Field
        },
        {
            path: "/compare/:id1,:id2",
            name: "compare2",
            component: Compare2
        },
        {
            path: "/origterm/:term",
            name: "origterm",
            component: OrigTerm
        },
        {
            path: "/filters",
            name: "DashOrig",
            component: DashOrig
        },
        {
            path: "/simple",
            name: "textwrap",
            component: TextWrap
        },
        {
            path: "/mlt/:id",
            name: "mltwrap",
            component: MltWrap
        }
        
        // route level code-splitting

        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        // component: () => import(/* webpackChunkName: "about"  "./views/About.vue")}

    ]
});
