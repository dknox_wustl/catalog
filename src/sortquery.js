export default function sortquery (paramsarg) {
    let s = "";
    let params = {...paramsarg};
    let sortdir = params["sortdir"];
    let sortby = params["sortby"];
    // console.log(sortdir);
    // console.log(sortby);
    
    switch (sortby) {
    case "":
        if (sortdir === "desc") {
            s = "&sort=score%20desc";
        }
        break;
    case "date":
        if (sortdir === "asc") {
            // s = "&sort=if(date_notBefore_l,0,1)%20asc,date_notBefore_l%20asc,date_notAfter_l%20asc,date_cert_s%20asc";
            s = "&sort=if(date_notBefore_l,1,0)%20asc,date_notBefore_l%20asc,date_notAfter_l%20asc,date_cert_s%20asc";
        } else {
            s = "&sort=date_notAfter_l%20desc,date_notBefore_l%20desc,date_cert_s%20asc";
        }
        break;
    case "author":
        break;
    case "title":
        break;
    default:
        
    }
    return s;
}

