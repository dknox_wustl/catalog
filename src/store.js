import Vue from "vue";
import Vuex from "vuex";
import VueResource from "vue-resource";
// import VueLodash from "vue-lodash";
import filterquery from "./filterquery.js";
import sortquery from "./sortquery.js";
// import filterquerynz from "./filterquerynz.js";
import router from "./router.js";

Vue.use(Vuex);
Vue.use(VueResource);
// Vue.use(VueLodash);

/* global _ */

const base =  (process.env.NODE_ENV === "production") ? "/solr_catalog/" :"http://localhost/solr_catalog/";

const baseselect = `${base}select`;

const emptyquery = {querytext: "", 
                    first: 0, 
                    filters: {},
                    nzfilters: []};

export default new Vuex.Store({
    state: {
        query: emptyquery,
        facetresponse: {},
        queryurl: "",
        getcountfacets: false,
        getsubjectfacets: true,
        fieldsorts: {},
        scatterurl: "",
        scatterresponse: {},
        base: base,
        exportset: []
    },
    getters: {
    },
    mutations: {
        updateFacetresponse(state, payload) {
            state.facetresponse = payload;
        },
        updateScatterresponse(state, payload) {
            state.scatterresponse = payload;
        },
        updateExportSet(state, payload) {
            state.exportset = payload;
        },
        setQueryText(state, payload) {
            if (payload === "" && this.state.route.query.hasOwnProperty("q")) {
                let queryclone = {... this.state.route.query};
                delete(queryclone["q"]);
                router.push({
                    query: queryclone
                });
            } else {
                let queryclone = {... this.state.route.query};
                queryclone["q"] = payload;
                router.push({
                    query: queryclone
                });
            }
            this.commit("setQueryFirst", 0);
        },
        // setLegacyFilter(state, payload) {
        //     let query = this.state.route.query;
        //     if (payload == "legacy") {
        //         if (query.hasOwnProperty("legacy")) {
                    
        //         }
        //     }
            
        // },
        addQueryFilter(state, payload) {
            let [field, value] = payload;
            let uniquevalfields = [
                "count_gap_l", "count_text_l", "count_lg_l", "count_l_l", 
                "count_hi_l", "count_p_l", "count_pb_l", "count_w_l", 
                "count_pc_l", "count_epigraph_l", "count_sp_l", "count_speaker_l", 
                "count_stage_l", "count_date_l", "count_head_l", "count_group_l", 
                "count_list_l", "count_front_l", "count_back_l", "count_body_l", 
                "count_div_l", "count_salute_l", "count_dateline_l", "count_opener_l", 
                "count_closer_l", "count_trailer_l"];
            if (value === "") {return; }
            if (this.state.route.query.hasOwnProperty(field) && 
                uniquevalfields.indexOf(field) === -1) {
                let qfieldlist = this.state.route.query[field].split("|");
                if (!qfieldlist.includes(encodeURI(value))) {
                    // console.log(qfieldlist);
                    // console.log(value, encodeURI(value));
                    let queryclone = {... this.state.route.query};
                    queryclone[field] += "|" + encodeURI(value);
                    router.push({
                        query: queryclone
                    });
                }
            } else {
                let queryclone = {... this.state.route.query};
                queryclone[field] = encodeURI([value]);
                router.push({
                    query: queryclone
                });
            }
            this.commit("setQueryFirst", 0);
        },
        //eslint-disable-next-line
        setURL(state) {
            // console.log("setURL", state.query);
            // console.log(state.route);
        },
        removeQueryFilter(state, payload) {
            let [field, value] = payload;
            value = decodeURI(value);
            if (this.state.route.query.hasOwnProperty(field)) {
                let qfieldlist = this.state.route.query[field].split("|");
                qfieldlist = qfieldlist.filter(v => v != encodeURI(value))
                    .join("|");
                let queryclone = {... this.state.route.query};
                if (qfieldlist) {
                    queryclone[field] = qfieldlist;
                } else {
                    delete(queryclone[field]);
                }
                router.push({
                    query: queryclone
                });
            }
            this.commit("setQueryFirst", 0);
        },
        addQueryNonzeroFilter(state, payload) {
            let field = payload;
            if (!state.query.nzfilters.includes(field)) {
                Vue.set(this.state.query, "nzfilters", 
                    state.query.nzfilters = state.query.nzfilters.concat(field));
            }
            this.commit("setQueryFirst", 0);
        },
        removeQueryNonzeroFilter(state, payload) {
            let field = payload;
            if (state.query.nzfilters.includes(field)) {
                Vue.set(this.state.query, "nzfilters", 
                    state.query.nzfilters.filter(v => v !== field));
            }
            this.commit("setQueryFirst", 0);
        },
        setQueryFirst(state, payload) {
            Vue.set(this.state.query, "first", payload);
        },
        setCountFacets(state, payload) {
            let prevstate = this.state.getcountfacets;
            if (prevstate != payload) {
                Vue.set(this.state, "getcountfacets", payload);
            }
        },
        setSubjectFacets(state, payload) {
            let prevstate = this.state.getsubjectfacets;
            if (prevstate != payload) {
                Vue.set(this.state, "getsubjectfacets", payload);
            }
        },
        setFieldSort(state, payload) {
            let field = payload[0];
            let sortbyindex = payload[1];
            let fieldsorts = this.state.fieldsorts;
            if (sortbyindex) {
                delete fieldsorts[field];
            } else {
                fieldsorts[field] = "count";
            }
            Vue.set(this.state, "fieldsorts", fieldsorts);
        }
    },
    actions: {
        routerPush (_, arg) {
            router.push(arg);
        },
        routerGo (_, arg) {
            router.go(arg);
        },
        getExportSet(context) {
            const baseurl = `${baseselect}?` + 
                  "&fl=count_*,date_*,sourceauthor_txt,sourcetitle_txt," + 
                  "tcpid_s&rows=61000";
            let q = "&q=*:*";
            if (this.state.route.query.q && this.state.route.query.q !== "") {
                q = `&q=matext:${this.state.route.query.q}`;
            }
            let fqs = filterquery(this.state.route.query);
            let sortqry = sortquery(this.state.route.query);
            let filteredurl = `${baseurl}${fqs}${q}${sortqry}`;
            this.state.exporturl = filteredurl;
            Vue.http.get(filteredurl)
                .then(response => {
                    context.commit("updateExportSet", response);
                }, response => {
                    context.commit("updateExportSet", response);
                });
        },
        getScatter(context) {
            const baseurl = `${baseselect}?` + 
                  "&fl=count_*&rows=61000";
            let q = "&q=*:*";
            if (this.state.route.query.q && this.state.route.query.q !== "") {
                q = `&q=matext:${this.state.route.query.q}`;
            }
            let fqs = filterquery(this.state.route.query);
            let sortqry = sortquery(this.state.route.query);
            let filteredurl = `${baseurl}${fqs}${q}${sortqry}`;
            this.state.scatterurl = filteredurl;
            Vue.http.get(filteredurl)
                .then(response => {
                    context.commit("updateScatterresponse", response);
                }, response => {
                    context.commit("updateScatterresponse", response);
                });
        },
        getFacets (context) {
            const baseurl = `${baseselect}?` + 
                  "facet.field=phase_s&facet.field=lang_s" + 
                  "&facet.field=epfinalGrade_s" + 
                  "&facet.field=eppublicationYear_txt" + 
                  "&facet.limit=-1" + 
                  "&facet=on&rows=10";
            const fields = [
                "count_*", "ep*", "*_s", "*_ss", "*_l", "id",
                "source*", "subj*", "*title_txt"];
            const fieldlist = _.join(fields, ",");
            const fl = `&fl=${fieldlist}`;
            const hl = "&hl=on&hl.fl=matext&hl.snippets=10&hl.maxAnalyzedChars=1000000";
            let q = "&q=*:*";
            if (this.state.route.query.q && this.state.route.query.q !== "") {
                q = `&q=matext:${this.state.route.query.q}`;
            }
            let start = (this.state.query.first > 0) ?
                `&start=${this.state.query.first}` : "&n=99";
            if (this.state.getcountfacets || this.state.getsubjectfacets) {
                q += "&facet.mincount=1";
            }
            if (this.state.getcountfacets) {
                "&facet.field=count_gap_l" + 
                    "&facet.field=count_text_l" + 
                    "&facet.field=count_lg_l" + 
                    "&facet.field=count_l_l" + 
                    "&facet.field=count_hi_l" + 
                    "&facet.field=count_p_l" + 
                    "&facet.field=count_pb_l" + 
                    "&facet.field=count_w_l" + 
                    "&facet.field=count_pc_l" + 
                    "&facet.field=count_epigraph_l" + 
                    "&facet.field=count_sp_l" + 
                    "&facet.field=count_speaker_l" + 
                    "&facet.field=count_stage_l" + 
                    "&facet.field=count_date_l" + 
                    "&facet.field=count_head_l" + 
                    "&facet.field=count_group_l" + 
                    "&facet.field=count_list_l" + 
                    "&facet.field=count_front_l" + 
                    "&facet.field=count_back_l" + 
                    "&facet.field=count_body_l" + 
                    "&facet.field=count_div_l" + 
                    "&facet.field=count_salute_l" + 
                    "&facet.field=count_dateline_l" + 
                    "&facet.field=count_opener_l" + 
                    "&facet.field=count_closer_l" + 
                    "&facet.field=count_trailer_l";
            }
            if (this.state.getsubjectfacets) {
                q += "&facet.field=subjectchron_ss" + 
                    "&facet.field=subjectcorp_ss" + 
                    "&facet.field=subjectevent_ss" + 
                    "&facet.field=subjectformgenre_ss" + 
                    "&facet.field=subjectgeog_ss" + 
                    "&facet.field=subjectpersonal_ss" + 
                    "&facet.field=subjecttitle_ss" + 
                    "&facet.field=subjecttopical_ss" +

                    "&facet.field=estc_general_ss" +
                    "&facet.field=estc_geographic_ss" +
                    "&facet.field=estc_temporal_ss" +
                    "&facet.field=estc_added_person_ss" +
                    "&facet.field=estc_creator_ss" +
                    "&facet.field=estc_subject_person_ss" +
                    "&facet.field=estc_form_ss" +
                    "&facet.field=estc_form_ss" +
                    "&facet.field=marc700_ss" +
                    "&facet.field=marc710_ss";
                for (let key in this.state.fieldsorts) {
                    let sortby = this.state.fieldsorts[key];
                    q += `&f.${key}.facet.sort=${sortby}`;
                }
            }
            let fqs = filterquery(this.state.route.query);
            let sortqry = sortquery(this.state.route.query);
            let filteredurl = `${baseurl}${fqs}${q}${start}${fl}${hl}${sortqry}`;
            // console.log(filteredurl);
            this.state.queryurl = filteredurl;
            Vue.http.get(filteredurl)
                .then(response => {
                    context.commit("updateFacetresponse", response);
                }, response => {
                    context.commit("updateFacetresponse", response);
                });
        }
    }
});
