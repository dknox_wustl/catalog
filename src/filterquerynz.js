//eslint-disable-next-line
// import VueLodash from "vue-lodash";
/* global _ */

export default function filterquerynz (nzfilters) {
    let segments = _.map(nzfilters, field => `${field}:[1 TO *]`);
    return " AND " +  _.join(segments, " AND ");
}
