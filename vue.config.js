module.exports = {
    runtimeCompiler: true,
    publicPath: process.env.NODE_ENV==='production'
	? '/catalog/'
	: '/',
    devServer: {
        host: 'localhost',
        port: 8081,
        https: false,
        hotOnly: false,
    }
}
