#!/usr/bin/env python

import requests
import re

r = requests.get("https://earlyprint.org")
txt = r.text

with open("ep.org", "w") as f:
   f.write(txt)

txt = open("ep.org").read()

start = txt.find("<header")
end = txt.find("</header>")

header = txt[start:end + len("</header>")]

header = re.sub('="/', '="https://earlyprint.org/', header)

script = """
<script>
export default {
    name: "shared-header"
};
</script>
"""
tmplt = "<template>" + header + "</template>" + script



with open("src/components/SharedHeader.vue", "w") as f:
    f.write(tmplt)

